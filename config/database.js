const mongoose = require('mongoose'); 

mongoose.connect('mongodb://localhost/demo1'); 
mongoose.connection
.once('open', () => console.log('Connection established'))
.on('error', (error) => {
    console.log('Warning : ' + error); 
});