const mongoose = require('mongoose'); 

mongoose.connect('mongodb://localhost/demo1'); 
mongoose.connection
.once('open', () => console.log('Connection established'))
.on('error', (error) => {
    console.log('Warning : ' + error); 
});
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name:String,
    email: String,
    fathername:String,
    postaladdress:String,
    personaladdress:String,
    gender:String,
    city:String,
    course:String,
    district:String,
    state:String,
    pincode:String,
    email:String,
    dob:Number,
    mobileno:Number
});
var USER= mongoose.model('user', UserSchema);
module.exports=USER;
