const express = require('express');
const bodyParser=require('body-parser');
const router      = express.Router();
const app = express();

const USER = require('./model/user');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/',(req,res)=>
{
res.sendFile(__dirname + "/index.html");
});

app.post('/signup',(req,res,next) =>{
    USER.create({
    name :req.body.name,
    fathername:req.body.fathername,
    postaladdress:req.body.paddress,
    personaladdress:req.body.personaladdress,
    gender:req.body.gender,
    city:req.body.City,
    course:req.body.Course,
    district:req.body.District,
    state:req.body.State,
    pincode:req.body.pincode,
    email: req.body.emailid,
    dob:req.body.DOB,
    mobileno:req.body.mobileno
    },function(err, users){
        if(err) 
        {
            res.json(err);
        }
        else
        {
            res.json(users);
        }
        
	});
});
app.post('/exists',(req,res)=>{
	USER.find({email:req.body.email}, function(err, user) {
        if (user.length === 0) {
            return res.status(403).send("email already exists");
          }
  
          return res.status(200).send("OK");
	});
});
    
app.listen(3000,()=>console.log("server is running on port 3000"));